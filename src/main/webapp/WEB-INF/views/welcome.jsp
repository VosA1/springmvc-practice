<%@ include file="common/header.jspf"%>
<%@ include file="common/navigation.jspf"%>
<div class="container">
	<spring:message code="welcome.caption" /> ${name}!
</div>

<%@ include file="common/footer.jspf"%>