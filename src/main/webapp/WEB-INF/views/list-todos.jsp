<%@ include file="common/header.jspf"%>
<%@ include file="common/navigation.jspf"%>
	<div class="container">
		<table class="table table-hover">
			<caption><spring:message code="todo.caption" /></caption>
			<thead>
				<tr>
					<th>Description</th>
					<th>Target Date</th>
					<th>Is Completed?</th>
					<th> </th>
				</tr>
			</thead>

			<tbody>
				<c:forEach items="${todos}" var="todo">
					<tr>
						<td>${todo.desc}</td>
						<td><fmt:formatDate pattern="yyyy/MM/dd"
								value="${todo.targetDate}" /></td>
						<td>${todo.isdone}</td>
						<td><a href= "/update-todo?id=${todo.id}"class = "btn btn-success"> Update </a> </a> </td>
						<td><a href= "/delete-todo?id=${todo.id}"class = "btn btn-danger"> Delete </a> </a> </td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<div>
			<a class="btn btn-success" href="/add-todo">Add</a>
		</div>
	</div>
<%@ include file="common/footer.jspf"%>