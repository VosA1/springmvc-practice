package com.todo.rest;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.todo.todo.Todo;
import com.todo.todo.TodoService;

@RestController
public class TodoRestController {

	@Autowired
	private TodoService service;
	
	//Couldn't get DELETE and POST methods to work when testing with Postman.
	//Might have been because the basic authentication field wasn't working
	//and the workaround wasn't correct. Changing the RequestMethod to GET
	//and then putting the value in the url shows it works. Maybe a unit test
	//class to ensure the call works would be a good way to findout
	
	@RequestMapping(value = "/listTodo", method = RequestMethod.GET)
	public ResponseEntity<List<Todo>> listAllTodos() {
		List<Todo> todos = service.retrieveTodos("Alex");
		if(todos.isEmpty()) {
			return new ResponseEntity<List<Todo>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Todo>>(todos, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/listTodo/{id}", method = RequestMethod.GET)
	public ResponseEntity<Todo> listAllTodosId(@PathVariable int id) {
		Todo todo = service.retrieveTodoById(id);
		return new ResponseEntity<Todo>(todo, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/deleteTodo/{id}", method = RequestMethod.DELETE) //Delete
	public ResponseEntity<Void> deleteTodo(@PathVariable int id, BindingResult result) {
		if (result.hasErrors()) {
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
		}
		service.deleteTodo(id);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
	
	@RequestMapping(value = "/addTodo", method = RequestMethod.POST) //Post
	public ResponseEntity<Void> addTodo(@RequestBody Todo todo, BindingResult result) {
		if (result.hasErrors()) {
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
		}
		service.addTodo(todo.getUser(), todo.getDesc(), todo.getTargetDate(), todo.isIsdone());
		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}
}
