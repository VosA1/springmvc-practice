# README #
Download zip/Clone repo and import as existing maven project. Run as maven build with goals tomcat7:run

### What is this repository for? ###
Practice spring mvc application. Basics of jsps(small amount of html with boostrap), model-view-controller 
Use of @Annotations (autowired, service, controller, restcontroller, sessionAttributes, request mapping, response entity, etc)

### Note ###
Only one user (Alex, pass)
Want to add DB interactions for multiple users, proper data access objects and Jdbc practice
Want to add more functionality so this has a purpose beyond practice